const mongoose = require('mongoose');

//Create user model
const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, "Firstname is required"]
    },
    lastName: {
        type: String,
        required: [true, "Lastname is required"]
    },
    email: {
        type: String,
        required: [true, "Email is required"],
        unique: [true, "Email is already registered"]
    },
    password: {
        type: String,
        required: [true, "Password is required"]
    },
    address: { //for delivery purposes
        type: String,
        required: [true, "Address is required"]
    },
    mobileNo: {
        type: String,
        required: [true, "Mobile number is required"]
    },
    isAdmin: {
        type: Boolean,
        default: false
    }
})

module.exports = mongoose.model('User', userSchema);
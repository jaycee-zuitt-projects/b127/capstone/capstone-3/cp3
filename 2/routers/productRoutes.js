const express = require('express');
const router = express.Router();
const productController = require('../controllers/productControllers');
const auth = require('../auth');

//creation of product (Admin only)
router.post('/createProduct', auth.verify, (req, res) => {
    const data = {
        product: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    if(data.isAdmin){
        productController.createProduct(data).then(result => res.send(result));
    }else{
        res.send(false)
    }
})

//retrieval of product
router.get('/allProducts', (req, res) => {
    productController.getAllProducts().then(result => res.send(result));
})

//retrieval of single product
router.get("/:productId", (req,res)=>{
	productController.specificProduct(req.params)
	.then(result =>{
		res.send(result)
	})
})

//modifying or updating product
router.put('/:productId', auth.verify, (req, res) => {
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    if(data.isAdmin){
        productController.modifyProduct(req.params, req.body).then(result => res.send(result));
    }else{
        res.send(false);
    }
})

//archive of product
router.put('/:productId/archive', auth.verify, (req, res) => {
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    if(data.isAdmin){
        productController.archiveProduct(req.params).then(result => res.send(result))
    }else{
        res.send(false);
    }
})

//get all active products
router.get("/", (req, res) => {
    productController.getActiveProducts().then(result => res.send(result));
})

module.exports = router;